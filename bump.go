package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {

	// os.Open returns both a file and an error, and we store them in the vars file and err
	file, err := os.Open("example.yaml")
	if err != nil {
		fmt.Println("error opening file")
		// Exit
		return
	}

	// Close the file after main() completes
	defer file.Close()

	// Scanner to read file line by line
	scanner := bufio.NewScanner(file)

	// iterate over the scanner
	for scanner.Scan() {
		// Grab our line
		line := scanner.Text()

		if strings.Contains(line, "image:") {
			clean_line := strings.Trim(line, " ")
			tokens := strings.Split(clean_line, ":")
			image_tag := tokens[2]
			fmt.Print(image_tag)

		}

	}

	// Check if there were errors during the scan
	if err := scanner.Err(); err != nil {
		fmt.Println("Error reading file:", err)
		return
	}
}
